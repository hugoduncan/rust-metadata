//! Provides a type wrapper that adds arbitrary metadata to a type.
//!
//! # Examples
//!
//! ```rust
//! #[macro_use] extern crate metadata;
//! use metadata::{Meta, MetaMut};
//!
//! pub struct Struct { pub f: u32 }
//! meta_type! { MetaStruct<String, String> = Struct }
//!
//! fn main() {
//!     let mut m = MetaStruct::new(Struct{f: 1});
//!     assert_eq!(1, m.f);     // deref
//!     m.f = 2;                // deref_mut
//!     assert_eq!(2, m.f);     // deref
//!
//!     m.meta_mut().insert("Hello".to_string(), "there".to_string());
//!     assert_eq!("there", *m.meta().get("Hello").unwrap());
//! }
//! ```

use std::collections::HashMap;

/// Trait for Metadata.  See [`meta_type!`](./macro.meta_type!.html) for s
/// macro to implement this on a type.
pub trait Meta<K, V> {
    /// Return a metadata map.
    fn meta(&self) -> &HashMap<K, V> ;
}

/// Trait for mutable Metadata.  See
/// [`meta_type!`](./macro.meta_type!.html) for s macro to implement
/// this on a type.
pub trait MetaMut<K, V> {
    /// Return a mutable metadata map.
    fn meta_mut(&mut self) -> &mut HashMap<K, V>;
}

/// Macro to declare a type-wrapper that adds a metadata map to a
/// type.  Optionally takes a sequence of comma separated `attr`
/// meta items, to be applied to the newtype as attributes.
///
/// Implements a `new` function for the type that accepts an
/// argument of an instance of the type it is wrapping.
///
/// # Examples
///
/// ```rust
/// #[macro_use] extern crate metadata;
///
/// #[derive(Debug, Clone)]
/// pub struct Struct { pub f: u32 }
///
/// meta_type! { MetaStruct<String, String> = Struct, derive(Debug, Clone) }
///
/// fn main() {
///     let mut m = MetaStruct::new(Struct{f: 1});
///     println!("{:?}", m);
///     assert_eq!(1, m.f);
/// }
/// ```
///
/// See [module docs](./index.html) for more details.
#[macro_export]
macro_rules! meta_type {
    ($mtype:ident < $k:ty, $v:ty > = $t:ty, $($attr:meta),*) => {
        $(#[$attr])*
        pub struct $mtype($t, ::std::collections::HashMap<$k, $v>);

        impl $mtype {
            #[doc="Create a new instance"]
            pub fn new(v: $t) -> $mtype {
                $mtype(v, ::std::collections::HashMap::new())
            }
        }


        impl $crate::Meta<$k, $v> for $mtype {
            fn meta(&self) -> &::std::collections::HashMap<$k, $v> {
                &self.1
            }
        }

        impl $crate::MetaMut<$k, $v> for $mtype {
            fn meta_mut(&mut self) -> &mut ::std::collections::HashMap<$k, $v> {
                &mut self.1
            }
        }

        impl ::std::ops::Deref for $mtype {
            type Target = $t;
            fn deref(&self) -> &$t {
                &self.0
            }
        }

        impl ::std::ops::DerefMut for $mtype {
            fn deref_mut(&mut self) -> &mut $t {
                &mut self.0
            }
        }
    };

    ($mtype:ident < $k:ty , $v:ty > = $t:ty) => {
        meta_type!($mtype<$k, $v> = $t,);
    };
}


#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Clone, Debug, PartialEq)]
    pub struct Struct {pub f: u32}

    meta_type! {
        MetaStruct<String, String > = Struct, derive(Clone,Debug,PartialEq)
    }

    meta_type! {
        MetaStruct2<String, String > = Struct // No attr clause
    }

    #[test]
    fn test_meta () {
        let mut m = MetaStruct::new(Struct{f: 1});
        assert_eq!(1, m.f);     // deref
        m.f = 2;                // deref_mut
        assert_eq!(2, m.f);     // deref

        m.meta_mut().insert("Hello".to_string(), "there".to_string());
        assert_eq!("there", *m.meta().get("Hello").unwrap());
        assert_eq!(m, m.clone());  // Clone implemented
        println!("{:?}", m);    // Deref implemented
    }

    fn test_mo_derive() {
        let m = MetaStruct2::new(Struct{f: 1});
        assert_eq!(1, m.f);     // deref
        // println!("{:?}", m);    // Deref not implemented
    }

}
